<%@ page import="java.time.LocalDate" %><%--
  Created by IntelliJ IDEA.
  User: suzan
  Date: 2019-02-07
  Time: 19:35
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<link rel="stylesheet" href="css/bootstrap.css"/>
<script src="js/bootstrap.js"></script>
<html>
<head>
    <title>Meta Horizon Java Quiz</title>
</head>
<body>
<jsp:include page="Header.jsp"/>

<h3 align="center">
    You can add more java questions to the list.
</h3>

<div class="container">
    <div class="panel panel-default">

        <form action="Show.jsp">

            <div class="form-group">
                <label for="Question">Question</label>
                <textarea type="text" class="form-control" name="question" id="question" rows="2"
                          placeholder="Type your question" required></textarea>
            </div>
            <div class="form-group">
                <label for="Option1">Option 1: </label>
                <textarea type="text" class="form-control" name="option1" rows="2" id="option1" placeholder="Type your option"
                          required></textarea>
            </div>
            <div class="form-group">
                <label for="Option2">Option 2: </label>
                <textarea type="text" class="form-control" name="option2" rows="2" id="option2" placeholder="Type your option"
                          required></textarea>
            </div>
            <div class="form-group">
                <label for="Option3">Option 3: </label>
                <textarea type="text" class="form-control" name="option3" rows="2" id="option3" placeholder="Type your option"
                          required></textarea>
            </div>
            <div class="form-group">
                <label for="Option4">Option 4: </label>
                <textarea type="text" class="form-control" name="option4" rows="2" id="option4" placeholder="Type your option"
                          required></textarea>
            </div>
            <div class="form-group">
                <label for="Answer">Answer: </label>
                <input type="text" class="form-control" name="answer" id="answer" placeholder="Type your answer" required>
            </div>
            <div class="form-group">
                <label for="Reason">Reason: </label>
                <textarea type="text" class="form-control" name="reason" rows="2" id="reason" placeholder="Type your reason"
                          required></textarea>
            </div>

            <div class="col-md-12 text-center">
                <button id="singlebutton" type="submit" name="singlebutton" class="btn btn-primary">SUBMIT</button>
            </div>
        </form>
    </div>
    <jsp:include page="Footer.jsp"/>
</div>
</body>
</html>