<%--
  Created by IntelliJ IDEA.
  User: suzan
  Date: 2019-02-09
  Time: 15:42
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<link rel="stylesheet" href="css/bootstrap.css"/>
<script src="js/bootstrap.js"></script>
<html>
<head>

</head>
<body>

<%
    String question = request.getParameter("question");
    String option1 = request.getParameter("option1");
    String option2 = request.getParameter("option2");
    String option3 = request.getParameter("option3");
    String option4 = request.getParameter("option4");
    String answer = request.getParameter("answer");
    String reason = request.getParameter("reason");
%>

<jsp:include page="Header.jsp"/>

<div class="container">
    <div class="panel panel-default">
        <p align="center">Following data has been added to the list of java questions.</p>
        <ul>
            <li><p><b>Question: </b><%=question%>
            </p></li>
            <li><p><b>Option A: </b><%=option1%>
            </p></li>
            <li><p><b>Option B: </b><%=option2%>
            </p></li>
            <li><p><b>Option C: </b><%=option3%>
            </p></li>
            <li><p><b>Option D: </b><%=option4%>
            </p></li>
            <li><p><b>Answer : </b><%=answer%>
            </p></li>
            <li><p><b>Reason : </b><%=reason%>
            </p></li>
        </ul>

    </div>
    <jsp:include page="Footer.jsp"/>
</div>


</body>
</html>
